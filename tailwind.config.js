/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    fontFamily: {
      cabin: ['Inter', 'sans-serif']
    }
  },
  plugins: [],
}
