import logo_hmj from "./hmj_assets/logo_hmj.jpeg"
import illustration1 from "./hmj_assets/illustration1.jpg"
import illustration2 from "./hmj_assets/illustration2.jpg"
import gambaralat1 from "./hmj_assets/gambaralat.png"
import img1A from "./hmj_assets/img1A.webp"
import img2A from "./hmj_assets/img2A.webp"
import img3A from "./hmj_assets/img3A.webp"
import img4A from "./hmj_assets/img4A.webp"

function App() {

  const menus = ['Tentang Kami','Produk','Kontak Kami'];
  return (
    <div className="bg-white">{/* parent NAVBAR */}
      <header className="bg-white container max-w-5xl mx-auto flex flex-row align-middle max-h-28 drop-shadow">
        <div>
          <img alt="logo_hmj" src={logo_hmj} className="w-24"/>
        </div>
        <div className="flex flex-auto align-middle items-center font-cabin text-sm">
          <ul className="flex flex-row space-x-5">
            {menus.map((val, index) => (
              <li key={index} className="hover:font-bold hover:cursor-default">{val}</li>
            ))}
          </ul>
        </div>
      </header>
      <main className="max-w-5xl mx-auto mt-1">
        <div>
          <h1 className="font-cabin font-bold text-6xl text-in-image text-white absolute">
            PERANGKAT HAMA<br/>TERPADU
          </h1>
        </div>
        <div className="max-w-5xl">
          <img alt="illustration1" src={illustration1}/>
        </div>
      </main>
      <profile className="profile-bg max-w-5xl mx-auto flex flex-row place-items-center">
        <div className="font-cabin top-16 left-28 px-24 py-16 tentang-kami">
          <h1 className="font-bold text-3xl pb-12">TENTANG KAMI</h1>
          <p className="text-base"><b>PT.HALIM MAKMUR JAYAABADI</b> adalah suatu perusahaan yang bergerak di bidang teknologi pertanian dan instrumentasi</p>
        </div>
        <div className="top-0 w-fit h-fit">
          <img alt="illustration2" src={illustration2}/>
        </div>
      </profile>
      <div className="produk max-w-5xl mx-auto flex flex-row place-items-center">{/* parent PRODUCT */}
        <div className="gambar-alat pl-44 pr-10">
          <img alt="gambaralat1" src={gambaralat1}/> 
        </div>
        <div className="text-right font-cabin pt-10 pr-10">
          <h2>Produk Kami</h2>
          <h1 className="font-bold">PERANGKAP HAMA TERPADU</h1>
          <p className="py-5">NAMA PRODUK merupakan produk inovasi teknnologi untuk pengendalian hama yang efektif, hemat energi, dan ramah lingkungan. Produk iini dapat diaplikasikan pada berbagai jenis lahan kawasan pertanian, mulai dari lahan persawahan, kawasan holtikultura, dan area perkebunan.</p>
        </div>
      </div>
      <div className = "max-w-5xl mx-auto flex flex-col">{/* parent HOW IT WORKS */}
        <div className="font-cabin font-semibold text-center place-content-center pt-20 mb-16">{/* title */}
          <h2 className="text-lg">HOW IT WORKS</h2>
          <h1 className="text-2xl">Perlindungan untuk tanaman kebun anda!</h1>
        </div>
        <div className="h-auto flex flex-row">{/* product description 1 */}
          <div className="w-3/5">
            <img alt="img1A" src={img1A}/>
          </div>
          <div className="w-2/5 font-cabin text-left background4 pt-20 px-24">
            <h1 className="font-bold text-base mb-3">MEMERANGKAP BERBAGAI JENIS HAMA TUMBUHAN</h1>
            <p className="text-xs">Produk kami dapat menarik serangga agar datang dan terperangkap menggunakan teknologi lampu perangkap(light trap), atraktan perangkap(attractant trap), lem perangkap(sticky trap) dan perangkap listrik(electrical trap).</p>
          </div>
        </div>
        <div className="h-auto flex flex-row">{/* product description 2 */}
          <div className="w-2/5 font-cabin text-left background4 pt-20 px-24">
            <h1 className="font-bold text-base mb-3">DAPAT BEROPERASI DI TEMPAT TANPA ALIRAN LISTRIK</h1>
            <p className="text-xs">Produk kami dilengkapi dengan solar panel berkapasitas 80 Wp dan baterai berkapasitas 120 Ah sehingga dapt digunakan di tempat yang tidak tersedia aliran listrik PLN(off-grid).</p>
          </div>
          <div className="w-3/5">
            <img alt="img2A" src={img2A}/>
          </div>
        </div>
        <div className="h-auto flex flex-row">{/* product description 3 */}
          <div className="w-3/5">
            <img alt="img3A" src={img3A}/>
          </div>
          <div className="w-2/5 font-cabin text-left background4 pt-20 px-24">
            <h1 className="font-bold text-base mb-3">BERFUNGSI SEBAGAI ALAT PERANGKAP DAN MONITORING(MULTI-TRAP)</h1>
            <p className="text-xs">Dibekali dengan teknologi internet of things(IoT), produk kami dapat memonitor keadaan sekitar alat dan memberikan informasi yang dapat diakses melalui website dashboard.</p>
          </div>
        </div>
        <div className="h-auto flex flex-row">{/* product description 4 */}
          <div className="w-2/5 font-cabin text-left background4 pt-20 px-24">
            <h1 className="font-bold text-base mb-3">KOKOH, AWET, RAMAH LINGKUNGAN</h1>
            <p className="text-xs">Struktur produk kami disusun dengan material berkualitas dan tentunya awet dan tahan di berbagai jenis lahan kawasan pertanian seperti persawahan, kws. holtikultura, dan area kebun.</p>
          </div>
          <div className="w-3/5">
            <img alt="img4A" src={img4A}/>
          </div>
        </div>
      </div>
    </div>
  );
}
export default App;
